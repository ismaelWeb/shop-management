package com.onebox.shopmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopManagementApplication {
	//comentario desde git
	public static void main(String[] args) {
		SpringApplication.run(ShopManagementApplication.class, args);
	}

}
